#
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

LOCAL_PATH := $(my-dir)

OFFLINE_IME_LANG_PACK_FILE_LIST := $(patsubst $(LOCAL_PATH)/share/ime/google/d3_lms/%,%,$(shell find $(LOCAL_PATH)/share/ -type f))
OFFLINE_VELVET_LANG_PACK_FILE_LIST := $(patsubst $(LOCAL_PATH)/srec/en-US/%,%,$(shell find $(LOCAL_PATH)/srec -type f))

define install-ime-lang-pack-files
include $$(CLEAR_VARS)
LOCAL_MODULE := $(1)
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_PRODUCT_MODULE := true
LOCAL_MODULE_PATH := $$(TARGET_OUT_PRODUCT)/usr/share/ime/google/d3_lms
LOCAL_SRC_FILES := share/ime/google/d3_lms/$$(LOCAL_MODULE)
include $$(BUILD_PREBUILT)
endef

define install-velvet-lang-pack-files
include $$(CLEAR_VARS)
LOCAL_MODULE := $(1)
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_PRODUCT_MODULE := true
LOCAL_MODULE_PATH := $$(TARGET_OUT_PRODUCT)/usr/srec/en-US
LOCAL_SRC_FILES := srec/en-US/$$(LOCAL_MODULE)
include $$(BUILD_PREBUILT)
endef

$(foreach fp, $(OFFLINE_IME_LANG_PACK_FILE_LIST),\
  $(eval $(call install-ime-lang-pack-files, $(fp))))

$(foreach fp, $(OFFLINE_VELVET_LANG_PACK_FILE_LIST),\
  $(eval $(call install-velvet-lang-pack-files, $(fp))))
